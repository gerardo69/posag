var base_url = $('#base_url').val();
var usuarioID = 0;
$(document).ready(function($) {
    $.validator.addMethod("usuarioexiste", function(value, element) {
        console.log(value);
        var respuestav;
        $.ajax({
            type: 'POST',
            url: base_url + 'Usuarios/validar',
            data: {
                Usuario: value
            },
            async: false,
            statusCode: {
                404: function(data) {
                    new PNotify({
                        title: 'Error!',
                        text: 'No se encuentra archivo',
                        type: 'error',
                        styling: 'bootstrap3'
                    });

                },
                500: function() {
                    new PNotify({
                        title: 'Error!',
                        text: '500',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            },
            success: function(data) {
                if (data == 1) {
                    if ($('#UsuarioID').val() > 0) {
                        respuestav = true;
                    } else {
                        respuestav = false;
                    }

                } else {
                    respuestav = true;
                }



            }
        });
        console.log(respuestav);
        return respuestav;
    });
    var form_register = $('#formusuarios');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            Usuario: {
                usuarioexiste: true,
                minlength: 5,
                required: true
            },
            contrasena: {
                minlength: 6,
                required: true

            },
            contrasenav: {
                equalTo: contrasena,
                required: true
            },

        },
        messages: {
            Usuario: {
                usuarioexiste: 'Seleccione otro nombre de usuario'
            }
        },

        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);

        },

        highlight: function(element) { // hightlight error inputs

            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    $('.btton-cancela').click(function(event) {
        location.reload(true);
    });
    $('.btton-save').click(function(event) {
        var $valid = form_register.valid();
        if ($valid) {
            var datos = form_register.serialize();
            $.ajax({
                type: 'POST',
                url: base_url + 'Usuarios/add',
                data: datos,
                async: false,
                statusCode: {
                    404: function(data) {
                        new PNotify({
                            title: 'Error!',
                            text: 'No se encuentra archivo',
                            type: 'error',
                            styling: 'bootstrap3'
                        });

                    },
                    500: function() {
                        new PNotify({
                            title: 'Error!',
                            text: '500',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                },
                success: function(data) {
                    console.log(data);
                    if (data==1) {
                        new PNotify({
                            title: 'Hecho!',
                            text: 'Guardado Correctamente',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    }else if(data==2){
                        new PNotify({
                            title: 'Error!',
                            text: 'Se a excedido el numero de usuarios permitidos',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }
                    

                }
            });
        }

    });
    $('.editar_cat').click(function(event) {
        var id = $(this).data('id');
        var usu = $(this).data('usu');
        var per = $(this).data('per');
        var pf = $(this).data('pf');
        $('#UsuarioID').val(id);
        $('#Usuario').val(usu);
        $('#contrasena').val('xxxxxx');
        $('#contrasenav').val('xxxxxx');
        $('.btton-save').html('Editar');
        $('#personalId option[value=' + per + ']').attr('selected', 'selected');
        $('#perfilId option[value=' + pf + ']').attr('selected', 'selected');
    });
    $('#data-tables').DataTable({
        fixedHeader: !0,
        "lengthChange": false,
        "searching": false,
        "info": false,
    });
});

function modal_eliminar(id) {
    usuarioID = id;
    $('#modaleliminar').modal();
}
function boton_eliminarusu() {
    $.ajax({
        type: 'POST',
        url: base_url + 'Usuarios/delete',
        data: {
            usu:usuarioID
        },
        async: false,
        statusCode: {
            404: function(data) {
                new PNotify({
                    title: 'Error!',
                    text: 'No se encuentra archivo',
                    type: 'error',
                    styling: 'bootstrap3'
                });

            },
            500: function() {
                new PNotify({
                    title: 'Error!',
                    text: '500',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        },
        success: function(data) {
            console.log(data);
            new PNotify({
                title: 'Hecho!',
                text: 'Eliminado Correctamente',
                type: 'success',
                styling: 'bootstrap3'
            });
            setTimeout(function() {
                location.reload();
            }, 2000);

        }
    });
}