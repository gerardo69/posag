var base_url=$('#base_url').val();
$(document).ready(function() {
    $('.bttonsttuscancelado').click(function(event) {
    $('#cancelado_modal').modal();

    $('#cper').val($(this).data("cpersonal"));
    $('#mot').val($(this).data("cmotivo"));
    $('#fec').val($(this).data("cfecha"));  
  });
    $('#data-tables').DataTable({
            fixedHeader: true,
            responsive: !0,
            "order": [[ 0, "desc" ]],
            "paging": false,
            "info": false,
            "lengthChange": false,
            "searching": false
        }); 
});
function modal_pago(id){
	$('#pago_modal').modal();
	$('#ventaId').val(id);
  $.ajax({ 
        type:'POST',
        url: base_url+'ListadoVentasCredito/pagos',
        data: {ventaId:id},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
              $('.tabla_pagos').html(data); 
        }
    });

}

function btn_agregar(){
  var pago = $('#pago').val();
  if(pago!=''){
      $.ajax({ 
        type:'POST',
        url: base_url+'ListadoVentasCredito/add',
        data: {ventaId:$("#ventaId").val(),pago:$('#pago').val()},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
             if(data == 1){
                new PNotify({
                      title: 'Verificar!',
                       text: 'Es mayor a la cantidad que debe',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
             }else{
              new PNotify({
                      title: 'Hecho!',
                       text: 'Guardado Correctamente',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
             setTimeout(function(){ 
                 $('#pago_modal').modal('hide');
                 location.href='';
                }, 1000);
             }
             
            
        }
    });
  }else{
    new PNotify({
        title: 'Verificar!',
         text: 'Ingrese una cantidad',
        type: 'error',
        styling: 'bootstrap3'
    });
  }
	
}
/// Modal de cancelacion
function modal_cancela(id){
  $('#cancelar_modal').modal();
  $('#ventaId_venta').val(id);
}  

function btn_cancelar(){
  $.ajax({ 
        type:'POST',
        url: base_url+'ListadoVentasCredito/cancelar',
        data: {ventaId:$("#ventaId_venta").val(),cancela_motivo:$('#cancela_motivo').val()},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){

              new PNotify({
                      title: 'Hecho!',
                       text: 'Guardado Correctamente',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
             setTimeout(function(){ 
                 location.href='';
                }, 1000);
        }
    });
}
