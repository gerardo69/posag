var base_url=$('#base_url').val();

$(document).ready(function($) {
	$('.saveform').click(function(event) {
		$.ajax({
            type:'POST',
            url: base_url+'Categoria/add',
            data: {
            	categoriaId:$('#categoriaId').val(),
            	categoria:$('#categoria').val()
            },
            async: false,
            statusCode:{
                404: function(data){
                    new PNotify({
                                  title: 'Error!',
                                  text: 'No se encuentra archivo',
                                  type: 'error',
                                  styling: 'bootstrap3'
                              });
                },
                500: function(){
                     new PNotify({
                                  title: 'Error!',
                                  text: '500',
                                  type: 'error',
                                  styling: 'bootstrap3'
                              });
                }
            },
            success:function(data){
                new PNotify({
                                  title: 'Hecho!',
                                  text: 'Guardado Correctamente',
                                  type: 'success',
                                  styling: 'bootstrap3'
                              });
                $('.modalcategoriaadd').modal('hide');
                setInterval(function(){ 
                            location.href='';
                        }, 3000);

                
                
            }
        });
	});	
	$('.editar_cat').click(function(event) {
		$('.modalcategoriaadd').modal();
		$('#categoriaId').val($(this).data("id"));
		$('#categoria').val($(this).data("valor"));
	});
	$('.cancelarform').click(function(event) {
		$('#categoriaId').val(0);
		$('#categoria').val('');
	});
});

function modal_eliminar(id,name){
    $('#eliminar_modal') .modal();
   // $(".eliminar_modal").slideToggle()
    $("#categoriaId").val(id);
    $(".nom").html('<b>'+name+'</b>');
}
function boton_eliminar(){
    $.ajax({ 
        type:'POST',
        url: base_url+'Categoria/eliminar',
        data: {categoriaId:$("#categoriaId").val()},
        async: false,
        statusCode:{
            404: function(data){
                new PNotify({
                      title: 'Error!',
                      text: 'No se encuentra archivo',
                      type: 'error',
                      styling: 'bootstrap3'
                  });

            },
            500: function(){
                new PNotify({
                      title: 'Error!',
                      text: '500',
                      type: 'error',
                      styling: 'bootstrap3'
                  });
            }
        },
        success:function(data){
             new PNotify({
                      title: 'Hecho!',
                       text: 'Eliminado Correctamente',
                      type: 'success',
                      styling: 'bootstrap3'
                  });
             setInterval(function(){ 
                   $('.cat_'+$("#categoriaId").val()).remove(); 
                }, 1000);
            
        }
    });
}