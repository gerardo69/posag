-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 03-06-2019 a las 17:12:43
-- Versión del servidor: 5.6.39-83.1
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `beaverso_pos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `bitacoraid` bigint(20) NOT NULL,
  `personalId` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `id_cambio` int(11) NOT NULL,
  `tipo_cambio` varchar(200) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `categoriaId` int(11) NOT NULL,
  `categoria` varchar(200) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`categoriaId`, `categoria`, `activo`, `reg`) VALUES
(1, 'Ninguno', 1, '2019-04-08 01:20:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `ClientesId` bigint(20) NOT NULL,
  `Nombre` varchar(250) NOT NULL,
  `Domicilio` text,
  `Ciudad` varchar(100) DEFAULT NULL,
  `Estado` varchar(100) DEFAULT NULL COMMENT 'Estado/Provincia/Region',
  `Pais` varchar(45) DEFAULT NULL,
  `CodigoPostal` varchar(45) DEFAULT NULL,
  `Correo` varchar(200) NOT NULL,
  `nombrec` varchar(100) NOT NULL,
  `correoc` varchar(200) NOT NULL,
  `telefonoc` varchar(30) NOT NULL,
  `extencionc` varchar(20) NOT NULL,
  `celular` varchar(30) NOT NULL,
  `descripcionc` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 activo 0 eliminado',
  `propietarioid` int(11) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ClientesId`, `Nombre`, `Domicilio`, `Ciudad`, `Estado`, `Pais`, `CodigoPostal`, `Correo`, `nombrec`, `correoc`, `telefonoc`, `extencionc`, `celular`, `descripcionc`, `activo`, `propietarioid`, `reg`) VALUES
(1, 'PUBLICO EN GENERAL', '', '', '', 'Mexico', '', '', '', '', '', '', '', '', 1, 0, '2019-04-26 22:24:22'),
(2, 'DULCIBOTANASsx', 'GUADALAJARA', 'PUEBLA', 'PUEBLA', 'Mexico', '72150', 'edreimagdiel@gmail.com', 'EDREI', 'edreimagdiel@gmail.com', '2225464434', '', '', '', 1, 0, '2019-04-26 22:24:22'),
(3, 'xxxxxxxx11', 'calle', 'municipio', 'estado', 'mexico', '94140', 'ddd@h.com', 'contacto', 'ccc', '111111111', '11', '222222', 'des', 1, 0, '2019-04-26 22:24:22'),
(4, 'ñkñkñk', 'k', 'k', 'k', 'M?xico', '94140', 'cas@hot.com', 'n', 'cas@h.com', '123456789', '123', '123456', 'fghjk', 1, 0, '2019-04-26 22:24:22'),
(5, 'hectoe lagunes loyo', 'conocida', 'conocido', 'veracruz', 'M?xico', '94140', 'lagunes@hotmal.com', 'yo', 'lagunes@hotmal.com', '12356789', '34', '2345678', 'sd?okfs?ldkfsd', 1, 0, '2019-04-26 22:24:22'),
(6, 'l', 'l', 'l', 'l', NULL, 'l', 'magiicirqus@gmail.com', 'l', 'magiicirqus@gmail.com', 'l', 'l', 'l', '<ol>\r\n	<li>lsdfd<strong>fsdf</strong>sdf<em>sdfs</em></li>\r\n	<li><em>555555</em></li>\r\n</ol>\r\n', 1, 0, '2019-04-26 22:24:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `compraId` int(11) NOT NULL,
  `sucursalid` int(11) NOT NULL DEFAULT '1',
  `id_proveedor` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `monto_total` float NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`compraId`, `sucursalid`, `id_proveedor`, `personalId`, `monto_total`, `activo`, `reg`) VALUES
(1, 1, 24, 1, 115, 1, '2019-05-14 12:44:40'),
(2, 1, 24, 1, 115, 1, '2019-05-14 12:46:34'),
(3, 1, 24, 1, 120, 1, '2019-05-14 12:48:41'),
(4, 1, 24, 1, 120, 1, '2019-05-14 12:50:01'),
(5, 1, 24, 1, 120, 1, '2019-05-14 12:55:25'),
(6, 1, 24, 1, 120, 1, '2019-05-14 13:02:59'),
(7, 1, 24, 1, 120, 1, '2019-05-14 13:03:59'),
(8, 1, 24, 1, 120, 1, '2019-05-14 13:07:44'),
(9, 1, 24, 1, 120, 1, '2019-05-14 13:07:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_detalles`
--

CREATE TABLE `compras_detalles` (
  `compradId` int(11) NOT NULL,
  `compraId` int(11) NOT NULL,
  `productoid` bigint(20) NOT NULL,
  `cantidad` float NOT NULL,
  `precio_compra` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `compras_detalles`
--

INSERT INTO `compras_detalles` (`compradId`, `compraId`, `productoid`, `cantidad`, `precio_compra`) VALUES
(1, 1, 1, 5, 23),
(2, 2, 1, 5, 23),
(3, 3, 1, 6, 20),
(4, 4, 1, 6, 20),
(5, 5, 1, 6, 20),
(6, 6, 1, 6, 20),
(7, 7, 1, 6, 20),
(8, 8, 1, 6, 20),
(9, 9, 1, 6, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id` int(11) NOT NULL,
  `usuariosmaximo` int(2) NOT NULL DEFAULT '4',
  `productosmaximos` int(11) NOT NULL DEFAULT '4000',
  `vigencia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `costomensual` float NOT NULL COMMENT '3% + 3 pesos',
  `costoanual` int(11) NOT NULL COMMENT '3% + 3 pesos'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id`, `usuariosmaximo`, `productosmaximos`, `vigencia`, `costomensual`, `costoanual`) VALUES
(1, 4, 4000, '2019-05-24 23:26:51', 240, 2578);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `gastosid` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `cantidad` float NOT NULL,
  `concepto` text NOT NULL,
  `sucursal` int(11) NOT NULL,
  `comentarios` text NOT NULL,
  `personalId` int(11) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1' COMMENT '1 vigente 0 cancelado',
  `personalId_cancela` int(11) DEFAULT NULL,
  `motivo_cancela` text,
  `reg_cancela` timestamp NULL DEFAULT NULL,
  `activod` int(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gastos`
--

INSERT INTO `gastos` (`gastosid`, `fecha`, `cantidad`, `concepto`, `sucursal`, `comentarios`, `personalId`, `activo`, `personalId_cancela`, `motivo_cancela`, `reg_cancela`, `activod`, `reg`) VALUES
(1, '2019-05-25', 500.1, 'sss', 1, 'yyyy', 1, 0, 1, 'xxx', '2019-05-09 03:00:00', 1, '2019-05-09 05:03:42'),
(2, '2019-05-10', 333, 'xxx', 1, 'xxx', 1, 0, 1, 'x', '2019-05-10 03:00:00', 1, '2019-05-10 16:00:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `Icon` varchar(45) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`) VALUES
(1, 'Catálogos', 'fa fa-book'),
(2, 'Operaciones', 'fa fa-folder-open'),
(3, 'Configuración\n', 'fa fa fa-cogs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_sub`
--

CREATE TABLE `menu_sub` (
  `MenusubId` int(11) NOT NULL,
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Pagina` varchar(120) CHARACTER SET latin1 DEFAULT NULL,
  `Icon` varchar(45) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu_sub`
--

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`) VALUES
(1, 1, 'Personal', 'Personal', 'fa fa-group'),
(2, 3, 'Categoria', 'Categoria', 'fa fa-cogs'),
(3, 1, 'Productos', 'Productos', 'fa fa-group'),
(4, 2, 'Ventas', 'Ventas', 'fa fa-shopping-cart'),
(5, 2, 'Compras', 'Compras', 'fa fa-cogs'),
(6, 1, 'Clientes', 'Clientes', 'fa fa-user'),
(7, 1, 'Proveedores', 'Proveedores', 'fa fa-truck'),
(8, 2, 'Reportes', 'Reportes', 'fa fa-cogs'),
(9, 2, 'Lista de ventas', '-', 'fa fa-cogs'),
(10, 2, 'Turno', '-', 'fa fa-cogs'),
(11, 2, 'Lista de turnos', '-', 'fa fa-cogs'),
(12, 2, 'Lista de compras', '-', 'fa fa-shopping-cart'),
(13, 3, 'Config. de ticket', 'Config_ticket', 'fa fa-cogs'),
(14, 2, 'Verificador', 'Verificador', 'fa fa-eye'),
(15, 2, 'Gastos', 'Gastos', 'fa fa-money'),
(16, 1, 'Sucursal', 'Sucursal', 'fa fa-briefcase'),
(17, 2, 'Listado Ventas Normales', 'ListadoVentasNormales', 'fa fa-bar-chart'),
(18, 2, 'Listado Ventas Credito', 'ListadoVentasCredito', 'fa fa-bar-chart'),
(19, 3, 'Usuarios', 'Usuarios', 'fa fa-user'),
(20, 3, 'Pagos', 'Pagos', 'fa fa-cogs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_credito`
--

CREATE TABLE `pagos_credito` (
  `pagoId` int(11) NOT NULL,
  `ventaId` bigint(20) NOT NULL,
  `personalId` int(11) NOT NULL,
  `pago` float NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `pagos_credito`
--

INSERT INTO `pagos_credito` (`pagoId`, `ventaId`, `personalId`, `pago`, `reg`) VALUES
(1, 5, 1, 10, '2019-05-14 03:22:15'),
(2, 5, 1, 10, '2019-05-14 03:23:28'),
(7, 5, 1, 1, '2019-05-16 11:31:33'),
(8, 5, 1, 2, '2019-05-16 11:31:52'),
(9, 5, 1, 1, '2019-05-16 11:32:17'),
(10, 6, 1, 10, '2019-05-27 01:32:30'),
(11, 6, 1, 15.5, '2019-05-27 01:33:44'),
(12, 6, 1, 38, '2019-05-27 01:34:20'),
(13, 7, 1, 44.6, '2019-05-27 14:14:06'),
(14, 8, 1, 100, '2019-05-29 13:52:05'),
(16, 8, 1, 100, '2019-05-29 14:00:27'),
(17, 8, 1, 1, '2019-06-01 20:40:34'),
(18, 8, 1, 4.2, '2019-06-01 20:43:28'),
(19, 6, 1, 1.3, '2019-06-01 20:43:48'),
(20, 11, 1, 10, '2019-06-02 22:51:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

CREATE TABLE `perfiles` (
  `perfilId` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`perfilId`, `nombre`) VALUES
(1, 'Administrador root'),
(2, 'Administrador'),
(3, 'Ventas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles_detalles`
--

CREATE TABLE `perfiles_detalles` (
  `Perfil_detalleId` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `MenusubId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfiles_detalles`
--

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 7),
(5, 1, 6),
(6, 1, 4),
(7, 1, 13),
(8, 1, 14),
(9, 1, 15),
(10, 1, 16),
(11, 1, 17),
(12, 1, 18),
(13, 1, 5),
(14, 1, 8),
(15, 1, 19),
(16, 1, 20),
(17, 2, 1),
(18, 2, 2),
(19, 2, 3),
(20, 2, 7),
(21, 2, 6),
(22, 2, 4),
(23, 2, 14),
(24, 2, 15),
(25, 2, 17),
(26, 2, 18),
(27, 2, 5),
(28, 2, 8),
(29, 2, 19),
(30, 3, 4),
(31, 3, 5),
(32, 3, 14),
(33, 3, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `personalId` int(11) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `domicilio` text NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT '1' COMMENT '0 administrador 1 normal',
  `sucursalId` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 visible 0 eliminado',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`personalId`, `nombre`, `domicilio`, `telefono`, `celular`, `correo`, `tipo`, `sucursalId`, `activo`, `reg`) VALUES
(1, 'Administrador', '', '', '', '', 0, 1, 1, '2019-03-18 20:10:27'),
(2, 'gerardo', 'conocido', '123456789', '1234567890', 'soporte@mangoo.mx', 1, 1, 1, '2019-03-18 20:10:27'),
(3, 'vvvvx', 'fffffffffff', '3435453453', '345345345', '353@h.com', 1, 1, 1, '2019-03-25 01:44:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `productoid` bigint(20) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `nombre` varchar(120) NOT NULL,
  `descripcion` text NOT NULL,
  `categoria` int(11) NOT NULL,
  `stock1` float NOT NULL DEFAULT '0',
  `stock2` float NOT NULL DEFAULT '0',
  `stock3` float NOT NULL DEFAULT '0',
  `preciocompra` float NOT NULL,
  `precioventa` float NOT NULL,
  `img` varchar(120) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1' COMMENT '1 actual 0 eliminado',
  `propietarioid` int(11) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`productoid`, `codigo`, `nombre`, `descripcion`, `categoria`, `stock1`, `stock2`, `stock3`, `preciocompra`, `precioventa`, `img`, `activo`, `propietarioid`, `reg`) VALUES
(1, '4567890', 'chetoss', 'chetos torcidos', 1, -5, 0, 0, 20, 24, '190421-160858catchetos.jpg', 1, 0, '2019-04-21 21:08:58'),
(2, '65546546', 'ñmñlmlk', 'lkmlk', 1, 11, 0, 0, 15, 15, '', 1, 0, '2019-05-13 03:20:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `razon_social` varchar(100) NOT NULL,
  `domicilio` varchar(100) NOT NULL COMMENT 'calle',
  `ciudad` varchar(50) NOT NULL,
  `estado` varchar(200) NOT NULL COMMENT 'estado, provincia, etc',
  `cp` varchar(8) NOT NULL,
  `telefono_local` varchar(10) NOT NULL,
  `telefono_celular` varchar(15) NOT NULL,
  `contacto` varchar(100) NOT NULL,
  `email_contacto` varchar(60) NOT NULL,
  `rfc` varchar(13) NOT NULL,
  `obser` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `razon_social`, `domicilio`, `ciudad`, `estado`, `cp`, `telefono_local`, `telefono_celular`, `contacto`, `email_contacto`, `rfc`, `obser`, `activo`) VALUES
(23, 'BIMBO S.A DE C.V', 'AAA', 'AAA', '', '12345', '123', '123', 'AAA', 'AAA', '\0HERD890308UA', '', 1),
(24, 'DULCERIA SUSY S.A DE C.V. SUCURSAL 3', '104 PTE NO 1720-A LOC 10 COL. ', 'PUEBLA', 'puebla', '876546', '8765432345', '2227535616', 'HECTOR', 'm@gmail.com', 'DSU910312LSO', 'asdasdasdasdass', 1),
(25, 'laknsldknaklsdnlkasd', '', '', '', '', '', '', '', '', '', '', 1),
(26, 'razon social2', 'domicilio2', 'ciudad2', '', '94140', '2345678', '123456789', 'contacto', 'email', '\0rfc', 'obser', 0),
(27, 'razon', 'domi', 'ciudad', '', '94140', '2345678', '23456789', 'contacto', 'email', '\0rfc', 'obser', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales`
--

CREATE TABLE `sucursales` (
  `sucursalid` int(11) NOT NULL,
  `logo` text NOT NULL,
  `sucursal` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `direccion` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursales`
--

INSERT INTO `sucursales` (`sucursalid`, `logo`, `sucursal`, `direccion`, `telefono`, `activo`) VALUES
(1, '190512-050848catcorrecto.jpg', 'sucursal1', 'CONOCIDA', '218-1184', 1),
(2, '', '', '', '', 0),
(3, '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `id_ticket` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `mensajea` text NOT NULL,
  `mensajeb` text NOT NULL,
  `fuente` varchar(20) NOT NULL,
  `margen_superior` int(11) NOT NULL,
  `tamanio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id_ticket`, `titulo`, `mensajea`, `mensajeb`, `fuente`, `margen_superior`, `tamanio`) VALUES
(1, 'titulo', '<p>QUEJAS Y SUJERENCIAS 01 800 000 0000</p>\r\n', '<p>Como te atendimos?</p>\r\n\r\n<p>YA AHORRASTE CON LOS PRECIOS MAS BAJOS</p>\r\n', 'courier', 10, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `UsuarioID` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `Usuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(80) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UsuarioID`, `perfilId`, `personalId`, `Usuario`, `contrasena`, `status`) VALUES
(1, 1, 1, 'admin', '$2y$10$.yBpfsTTEmhYhECki.qy3eL45XaLo3MinuIPgiPsjmXBYItZXHUga', 1),
(2, 3, 2, 'ventas', '$2y$10$KvKERCn4vuxDS5OvHZ/eP./qYw.j45iqhs35x7FmUV0dl5oNGqiYC', 1),
(3, 2, 3, 'admins', '$2y$10$VmB1NVnb0A9GCrBG1sDE9.RGhWahKkKkHRdImRKOlww2xBSLGe0qG', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `ventaId` bigint(20) NOT NULL,
  `sucursalid` int(11) NOT NULL DEFAULT '1',
  `personalId` int(11) NOT NULL,
  `ClientesId` bigint(20) NOT NULL,
  `subtotal` float NOT NULL,
  `ndescuento` float NOT NULL,
  `descuento` float NOT NULL,
  `total` float NOT NULL,
  `tipopago` int(1) NOT NULL DEFAULT '1' COMMENT '1 contado 2 credito',
  `fechavencimiento` date DEFAULT NULL COMMENT 'la fecha de vencimiento en caso de ser credito',
  `pagado` int(1) NOT NULL DEFAULT '1' COMMENT '1 pagado, 0 no cubierto en el caso de los creditos',
  `metodo` int(1) NOT NULL DEFAULT '1' COMMENT '1 efectivo, 2 credito, 3 debito',
  `cancelado` int(1) NOT NULL DEFAULT '0',
  `cancela_personal` int(11) DEFAULT NULL,
  `cancela_motivo` text,
  `canceladoh` timestamp NULL DEFAULT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`ventaId`, `sucursalid`, `personalId`, `ClientesId`, `subtotal`, `ndescuento`, `descuento`, `total`, `tipopago`, `fechavencimiento`, `pagado`, `metodo`, `cancelado`, `cancela_personal`, `cancela_motivo`, `canceladoh`, `activo`, `reg`) VALUES
(1, 1, 1, 1, 24, 0, 0, 24, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-06 01:51:04'),
(2, 1, 1, 1, 24, 0, 0, 24, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-11 20:34:14'),
(3, 1, 1, 1, 24, 0, 0, 24, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-11 20:37:27'),
(4, 1, 1, 1, 72, 0, 0, 72, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-11 20:39:10'),
(5, 1, 1, 1, 24, 0, 0, 24, 2, '2019-05-31', 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-12 02:01:45'),
(6, 1, 1, 1, 72, 0.1, 7.2, 64.8, 2, '2019-05-27', 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-27 03:31:48'),
(7, 1, 1, 1, 48, 0.05, 2.4, 45.6, 2, '0000-00-00', 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-27 16:13:20'),
(8, 1, 1, 1, 216, 0.05, 10.8, 205.2, 2, '2019-05-29', 1, 1, 0, NULL, NULL, NULL, 1, '2019-05-29 15:49:31'),
(9, 1, 1, 1, 24, 0, 0, 24, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-06-02 22:05:12'),
(10, 1, 1, 1, 24, 0, 0, 24, 1, NULL, 1, 1, 0, NULL, NULL, NULL, 1, '2019-06-02 22:57:53'),
(11, 1, 1, 1, 24, 0.05, 1.2, 22.8, 2, '2019-06-29', 0, 1, 0, NULL, NULL, NULL, 1, '2019-06-03 00:50:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `ventadId` bigint(20) NOT NULL,
  `ventaId` bigint(20) NOT NULL,
  `productoid` bigint(20) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  `precioc` float NOT NULL COMMENT 'es el precio de compra en el momento de que se realizo la venta'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `venta_detalle`
--

INSERT INTO `venta_detalle` (`ventadId`, `ventaId`, `productoid`, `cantidad`, `precio`, `precioc`) VALUES
(1, 1, 1, 1, 24, 23),
(2, 2, 1, 1, 24, 23),
(3, 3, 1, 1, 24, 23),
(4, 4, 1, 3, 24, 23),
(5, 5, 1, 1, 24, 23),
(6, 6, 1, 3, 24, 20),
(7, 7, 1, 2, 24, 20),
(8, 8, 1, 9, 24, 20),
(9, 9, 1, 1, 24, 20),
(10, 10, 1, 1, 24, 20),
(11, 11, 1, 1, 24, 20);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`bitacoraid`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`categoriaId`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`ClientesId`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`compraId`),
  ADD KEY `compra_fk_proveedor` (`id_proveedor`),
  ADD KEY `compra_fk_personal` (`personalId`),
  ADD KEY `compras_fk_sucursal` (`sucursalid`);

--
-- Indices de la tabla `compras_detalles`
--
ALTER TABLE `compras_detalles`
  ADD PRIMARY KEY (`compradId`),
  ADD KEY `compra_fk_compra` (`compraId`),
  ADD KEY `compra_fk_productos` (`productoid`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`gastosid`),
  ADD KEY `gastos_fk_sucursal` (`sucursal`),
  ADD KEY `gastos_fk_personal` (`personalId`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`MenuId`);

--
-- Indices de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD PRIMARY KEY (`MenusubId`),
  ADD KEY `fk_menu_sub_menu_idx` (`MenuId`);

--
-- Indices de la tabla `pagos_credito`
--
ALTER TABLE `pagos_credito`
  ADD PRIMARY KEY (`pagoId`),
  ADD KEY `pago_fk_venta` (`ventaId`),
  ADD KEY `pago_fk_personal` (`personalId`);

--
-- Indices de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  ADD PRIMARY KEY (`perfilId`);

--
-- Indices de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD PRIMARY KEY (`Perfil_detalleId`),
  ADD KEY `fk_Perfiles_detalles_Perfiles1_idx` (`perfilId`),
  ADD KEY `fk_Perfiles_detalles_menu_sub1_idx` (`MenusubId`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`personalId`),
  ADD KEY `personal_fk_sucursal` (`sucursalId`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`productoid`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  ADD PRIMARY KEY (`sucursalid`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id_ticket`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`UsuarioID`),
  ADD KEY `fk_usuarios_Perfiles1_idx` (`perfilId`),
  ADD KEY `fk_usuarios_personal1_idx` (`personalId`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`ventaId`),
  ADD KEY `ventas_fk_personal` (`personalId`),
  ADD KEY `ventas_fk_clientes` (`ClientesId`),
  ADD KEY `ventas_fk_sucursal` (`sucursalid`);

--
-- Indices de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`ventadId`),
  ADD KEY `detallev_fk_productos` (`productoid`),
  ADD KEY `ventasd_fk_ventas` (`ventaId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `bitacoraid` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `categoriaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `ClientesId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `compraId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `compras_detalles`
--
ALTER TABLE `compras_detalles`
  MODIFY `compradId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `gastosid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `MenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  MODIFY `MenusubId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `pagos_credito`
--
ALTER TABLE `pagos_credito`
  MODIFY `pagoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  MODIFY `perfilId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  MODIFY `Perfil_detalleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `personal`
--
ALTER TABLE `personal`
  MODIFY `personalId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `productoid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  MODIFY `sucursalid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id_ticket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `UsuarioID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `ventaId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  MODIFY `ventadId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `compra_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `compra_fk_proveedor` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `compras_fk_sucursal` FOREIGN KEY (`sucursalid`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compras_detalles`
--
ALTER TABLE `compras_detalles`
  ADD CONSTRAINT `compra_fk_compra` FOREIGN KEY (`compraId`) REFERENCES `compras` (`compraId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `compra_fk_productos` FOREIGN KEY (`productoid`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD CONSTRAINT `gastos_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `gastos_fk_sucursal` FOREIGN KEY (`sucursal`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pagos_credito`
--
ALTER TABLE `pagos_credito`
  ADD CONSTRAINT `pago_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `pago_fk_venta` FOREIGN KEY (`ventaId`) REFERENCES `ventas` (`ventaId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD CONSTRAINT `perfil_fk_menu` FOREIGN KEY (`MenusubId`) REFERENCES `menu_sub` (`MenusubId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `perfil_fk_perfil` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal`
--
ALTER TABLE `personal`
  ADD CONSTRAINT `personal_fk_sucursal` FOREIGN KEY (`sucursalId`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuario_fk_perfil` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `usuario_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `ventas_fk_clientes` FOREIGN KEY (`ClientesId`) REFERENCES `clientes` (`ClientesId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `ventas_fk_personal` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `ventas_fk_sucursal` FOREIGN KEY (`sucursalid`) REFERENCES `sucursales` (`sucursalid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `detallev_fk_productos` FOREIGN KEY (`productoid`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `ventasd_fk_ventas` FOREIGN KEY (`ventaId`) REFERENCES `ventas` (`ventaId`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
