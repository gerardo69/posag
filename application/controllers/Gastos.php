<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gastos extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->personal=$this->session->userdata('idpersonal');
            $this->sucursalId=$this->session->userdata('sucursalId');
            if ($this->perfilid==1) {
                $this->sucursalId=0;
                $this->sucursalIdd=$this->sucursalId;
            }else{
                $this->sucursalIdd=$this->sucursalId;
            }
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,15);// 15 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }
	public function index(){
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            //ira el permiso del modulo
        }
        //====================================
            $pages=10;
            if (isset($_GET['search'])) {
                $buscar=$_GET['search'];
            }else{
                $buscar='';
            }

            $data['buscar']=$buscar;
            $this->load->library('pagination');
            $config['base_url'] = base_url().'Gastos/view';
            $config['total_rows'] = $this->ModeloCatalogos->filastotal_gastos($buscar,$this->sucursalId);
            $config['per_page'] = $pages;
            $this->pagination->initialize($config);
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["Gastos"] = $this->ModeloCatalogos->List_table_gastos($pagex,$config['per_page'],$buscar,$this->sucursalId);

    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('gastos/gastoslist',$data);
        $this->load->view('templates/footer');
        $this->load->view('gastos/gastoslistjs');
	}

    function Gastosadd(){
        $data['sucursalIdd']=$this->sucursalIdd;
        $data['perfilid']=$this->perfilid;
        $data['sucursal']=$this->ModeloCatalogos->getselectvalue1rowwhere('sucursales','activo',1);
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('gastos/gastosadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('gastos/gastoslistjs');
    }

    function cancelar(){
        $id = $this->input->post('idgasto');
        $motivo = $this->input->post('motivo_cancela');;
        $data = array('activo' => 0,'personalId_cancela' => $this->personal, 'motivo_cancela' => $motivo['motivo_cancela'],'reg_cancela'=>$this->fechahoy);
        $this->ModeloCatalogos->updateCatalogo('gastos',$data,' gastosid',$id);
    }
    function add(){
        $data = $this->input->post();
        $data['personalId'] = $this->personal;
        $this->ModeloCatalogos->Insert('gastos',$data);
    }

}    