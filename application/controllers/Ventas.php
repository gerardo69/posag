<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        /*
        $this->load->model('ModeloClientes');
        $this->load->model('ModeloVentas');*/
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloCatalogos');
        if ($this->session->userdata('logeado')) {
            $this->personal=$this->session->userdata('idpersonal');
            $this->sucursalId=$this->session->userdata('sucursalId');
             $this->perfilid=$this->session->userdata('perfilid');
        }else{
            redirect('Login');
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }
	public function index(){
        $data['sucursalesrow']=$this->ModeloCatalogos->getselectvalue1rowwhere('sucursales','activo',1);
        $data['clientedefault']=$this->ModeloCatalogos->getselectvalue1rowwhere('clientes','ClientesId',1);
        $data['fecha_actual']=$this->fechahoy;
        $result=$this->ModeloCatalogos->getselectvalue1rowwhere('configuracion','id',1);
        foreach ($result->result() as $item) {
            $fecha_v= $item->vigencia;
        }
        $data['fecha_vigencia'] = date("d/m/Y G:i:s", strtotime($fecha_v));
        $data['fecha_v'] = $fecha_v;
        $data['perfilid'] = $this->perfilid;
        if ($this->perfilid==1) {
            $data['list_block']='';
        }else{
           $data['list_block']='style="display:none;"'; 
        }
        
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('ventas/ventas',$data);
        $this->load->view('templates/footer');
        $this->load->view('ventas/ventasjs');
        //==================================
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
	}
    public function addproducto(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $where = array('productoid' => $prod);
        $personalv=$this->ModeloCatalogos->getselectvalue1rowwheren('productos',$where);
        //$oProducto = new Producto();
        foreach ($personalv->result() as $item){
            $id = $item->productoid;
            $codigo = $item->codigo;
            $nombre = $item->nombre;

            //========================
                $preciosucursal=array('idsucursal'=>$this->sucursalId,'idproducto'=>$prod);
                $productosprecios=$this->ModeloCatalogos->getselectvalue1rowwheren('productos_sucursales',$preciosucursal);
                $precio_venta=0;
                $mayoreo=0;
                $can_mayoreo=0;
                foreach ($productosprecios->result() as $itemp) {
                    $precio_venta=$itemp->precio_venta;
                    $mayoreo=$itemp->mayoreo;
                    $can_mayoreo=$itemp->can_mayoreo;
                }
            //========================
              /*
              $mediomayoreo = $item->mediomayoreo;
              $canmediomayoreo = $item->canmediomayoreo;
              $mayoreo = $item->mayoreo;
              $canmayoreo = $item->canmayoreo;
              */
            //$oProducto=array("id"=>$id,"codigo"=>$codigo,"nombre"=>$nombre,'precioventa'=>$precioventa,'mediomayoreo'=>$mediomayoreo,'canmediomayoreo'=>$canmediomayoreo,'mayoreo'=>$mayoreo,'canmayoreo'=>$canmayoreo);
            $oProducto=array(
                            "id"=>$id,
                            "codigo"=>$codigo,
                            "nombre"=>$nombre,
                            'precioventa'=>$precio_venta,
                            'mayoreo'=>$mayoreo,
                            'can_mayoreo'=>$can_mayoreo
                        );

        }
        if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
            $idx = array_search($oProducto, $_SESSION['pro']);
            $_SESSION['can'][$idx]+=$cant;
        }
        else{ //sino lo agrega
            array_push($_SESSION['pro'],$oProducto);
            array_push($_SESSION['can'],$cant);
        }
        //======================================================================
        $count = 0;
        $countpro=1;
        $n =array_sum($_SESSION['can']);
        foreach ($_SESSION['pro'] as $fila){
            $Cantidad=$_SESSION['can'][$count]; 

            if($fila['can_mayoreo']==0){
                $precio=$fila['precioventa'];
            }elseif ($Cantidad>=$fila['can_mayoreo']) {
                $precio=$fila['mayoreo'];
            }else{
                $precio=$fila['precioventa'];
            }
       
            //$precio=$fila['precioventa'];
        
        $cantotal=$Cantidad*$precio;
        ?>
            <tr class="producto_<?php echo $count;?>">   

                <td>
                    <input type="hidden" name="vsproid" id="vsproid" value="<?php echo $fila['id'];?>">
                    <?php echo $countpro;?>
                </td>   
                <td><?php echo $fila['nombre'];?></td>                                    
                <td>
                    <input type="number" name="vscanti" id="vscanti" value="<?php echo $Cantidad;?>" readonly style="background: transparent;border: 0px; width: 80px;">
                </td>                                        
                                                      
                <td>$ <input type="text" name="vsprecio" id="vsprecio" value="<?php echo $precio;?>" readonly style="background: transparent;border: 0px;width: 100px;"></td>                                        
                <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="<?php echo $cantotal;?>" readonly style="background: transparent;border: 0px;    width: 100px;"></td>                                        
                <td>                                            
                    <button type="button" class="btn btn-danger" title="Eliminar" onclick="deletepro(<?php echo $count;?>)"><i class="fa fa-minus-circle"></i> </button>
                </td>
            </tr>
        <?php
        $countpro++;
        $count++;
        }
    }
    function deleteproducto(){
        $idd = $this->input->post('idd');
        unset($_SESSION['pro'][$idd]);
        $_SESSION['pro']=array_values($_SESSION['pro']);
        unset($_SESSION['can'][$idd]);
        $_SESSION['can'] = array_values($_SESSION['can']); 
    }
    function productoclear(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
    }
    function ingresarventa(){
        $data = $this->input->post();
        $data['personalId']=$this->personal;
        if ($this->personal==1) {
            $data['sucursalid']=$data['sucursal'];
        }else{
            $data['sucursalid']=$this->sucursalId;
        }
        unset($data['sucursal']);
        $tipopago=$data['tipopago'];
        if ($tipopago==2) {
            $data['pagado']=0;
        }else{
            unset($data['fechavencimiento']);
        }
        $id=$this->ModeloCatalogos->Insert('ventas',$data);
        echo $id;
    }
    function ingresarventapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $data['ventaId'] = $DATA[$i]->ventaId;
            $productoid=$DATA[$i]->productoid;
            $data['productoid'] = $productoid;
            $cantidad = $DATA[$i]->cantidad;
            $data['cantidad'] = $DATA[$i]->cantidad;
            $data['precio'] = $DATA[$i]->precio;
            $data['precioc'] = $this->ModeloProductos->getproductospcompra($productoid);
            $id=$this->ModeloCatalogos->Insert('venta_detalle',$data);

            $this->ModeloCatalogos->updatestock2('productos_sucursales','existencia','-',$cantidad,'idproducto',$productoid,'idsucursal',$this->sucursalId);
        }
    }
    function vigencia(){
        $data = $this->input->post('fecha');
        $result=$this->ModeloCatalogos->getselectvalue1rowwhere('configuracion','id',1);
        foreach ($result->result() as $item) {
            $fecha_v= $item->vigencia;
        }
        $fecha1 = date_create($data);
        $fecha2 = date_create($fecha_v);
        $intevalo = date_diff($fecha1,$fecha2);
        $dias = (array)$intevalo;
        $d1= implode(":", $dias);
        $d2 = explode(":", $d1);
        $d3 = $d2[10];
        $x = (int)$d3;
        echo $x;
    }
    public function Activar(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('ventas/bloqueo');
        $this->load->view('templates/footer');
    }
    function viewpeocat(){

        $html='<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">';
        $html.='</div>';
        $html.='<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">';
        $html.='<button type="button" class="btn btn-primary" onclick="tactilcerrar();">Cerrar</button>';
        $html.='</div>';
        $resultado=$this->ModeloProductos->getcategoriasactive();
        foreach ($resultado as $item) {
            $html.='<div class="car-categorias" onclick="viewproduct('.$item->idcat.')"><p>'.$item->categoria.'</p></div>';
        }
        echo $html;
    }
    function viewproduct(){
        $cat = $this->input->post('categoria');
        $where = array('categoria' => $cat,'activo'=>1);
        $resultado=$this->ModeloCatalogos->getselectvalue1rowwheren('productos',$where);
        $html='<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10"><button type="button" class="btn btn-primary" onclick="viecategoria();">Regresar</button></div>';
        $html.='<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">';
        $html.='<button type="button" class="btn btn-primary" onclick="tactilcerrar();">Cerrar</button>';
        $html.='</div>';
        foreach ($resultado->result() as $item) {
            if ($item->img!='') {
                $img=base_url().'public/images/productost/'.$item->img;
              }else{
                $img=base_url().'public/images/pos.svg';
              }
            $html.='<div class="car-productos" onclick="productselected('.$item->productoid.')" ';
            $html.='style="background: white url('.$img.');';
            $html.='background-size: 65%;';
            $html.='background-position-x: center;';
            $html.='background-repeat: no-repeat;';
            $html.='"';
            $html.='><p>'.$item->nombre.'</p></div>';
        }
        echo $html;
    }
    
}
