<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            if ($this->perfilid==1) {
                $this->sucursalId=0;
            }
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,1);// 1 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }

    }
	public function index(){
        
        //====================================
            $pages=10;
            if (isset($_GET['search'])) {
                $buscar=$_GET['search'];
            }else{
                $buscar='';
            }
            $this->load->library('pagination');
            $config['base_url'] = base_url().'Personal/view';
            $config['total_rows'] = $this->ModeloCatalogos->filastotal_personal($buscar,$this->sucursalId);
            $config['per_page'] = $pages;
            $this->pagination->initialize($config);
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["Personal"] = $this->ModeloCatalogos->List_table_personal($pagex,$config['per_page'],$buscar,$this->sucursalId);
        //====================================




		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('personal/personallist',$data);
        $this->load->view('templates/footer');
        $this->load->view('personal/personaladdjs');     
	}
    function Personaladd($id=0){
        if ($id==0) {
            $data['subtitle']='Nuevo';
            $data['id']=$id;
            $data['button']='Guardar';
            $data['nombre']='';
            $data['sucursalId']=1;
            $data['domicilio']='';
            $data['telefono']='';
            $data['celular']='';
            $data['correo']='';
        }else{
            $result=$this->ModeloCatalogos->getselectvalue1rowwhere('personal','personalId',$id);
            foreach ($result->result() as $row) {
                $data['personalId']=$row->personalId;
                $data['nombre']=$row->nombre;
                $data['domicilio']=$row->domicilio;
                $data['telefono']=$row->telefono;
                $data['celular']=$row->celular;
                $data['correo']=$row->correo;
                $data['sucursalId']=$row->sucursalId;
            }
            $data['subtitle']='Editar';
            $data['id']=$id;
            $data['button']='Actualizar';
            
        }
        $where=array('activo'=>1);
        $data['sucursales']=$this->ModeloCatalogos->getselectvalue1rowwheren('sucursales',$where);
        if ($this->perfilid!=1) {
            $data['sucursalview']='style="display:none;"';
        }else{
            $data['sucursalview']='';
        }
        
        
        
        
        
        


        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('personal/personaladd',$data);
        $this->load->view('templates/footer');
        $this->load->view('personal/personaladdjs');
    }
    function add(){
        $data = $this->input->post();
        //echo $data;
        $id=$data['personalId'];
        unset($data['personalId']);
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo('personal',$data,'personalId',$id);
        }else{
            //echo $data;
            $this->ModeloCatalogos->Insert('personal',$data);
        }
    }

    public function eliminar(){
        $id = $this->input->post('personalId');
        $data = array('activo' => 0);
        $this->ModeloCatalogos->updateCatalogo('personal',$data,'personalId',$id);
    }

}