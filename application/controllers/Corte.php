<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corte extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloReportes');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            if ($this->perfilid==1) {
                $this->sucursalId=0;
            }
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,8);// 8 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('corte/corte');
        $this->load->view('templates/footer');
        $this->load->view('corte/cortejs');        
	}
    function reportes(){
        $fechaini = $this->input->post('fechaini');
        $fechafin = $this->input->post('fechafin');
        $tipo = $this->input->post('tipo');
        $datos='';

        switch ($tipo) {
            case '1':
                $total=0;
                $totale=0;
                $totalc=0;
                $totald=0;
                $resultado=$this->ModeloReportes->getventascontado($fechaini,$fechafin,$this->sucursalId);
                $datos.='<div class="row">';
                    $datos.='<div class="col-md-12" >';
                                $datos.='<h2 style="text-align:center;">VENTAS</h2>';
                    $datos.='</div>';
                $datos.='</div>';
                $datos.='<table class="table table-striped jambo_table bulk_action" id="data-tables">';
                    $datos.='<thead>';
                      $datos.='<tr >';
                        $datos.='<th>#</th>';
                        $datos.='<th>Cajero</th>';
                        $datos.='<th>Cliente</th>';
                        $datos.='<th>Fecha</th>';
                        $datos.='<th>SubTotal</th>';
                        $datos.='<th>Descuento</th>';
                        $datos.='<th>Total</th>';
                        $datos.='<th>Metodo</th>';
                        $datos.='<th></th>';
                      $datos.='</tr>';
                    $datos.='</thead>';
                    $datos.='<tbody>';
                    foreach ($resultado->result() as $item){
                        $datos.='<tr >';
                            $datos.='<td>'.$item->ventaId.'</td>';
                            $datos.='<td>'.$item->cajero.'</td>';
                            $datos.='<td>'.$item->cliente.'</td>';
                            $datos.='<td>'.$item->reg.'</td>';
                            $datos.='<td>'.$item->subtotal.'</td>';
                            $datos.='<td>'.$item->descuento.'</td>';
                            $datos.='<td>'.$item->total.'</td>';
                            if ($item->metodo==1) {
                                $metodo='Efectivo';
                                $totale=$totale+$item->total;
                            }elseif ($item->metodo==2) {
                                $metodo='Tarjeta de credito';
                                $totalc=$totalc+$item->total;
                            }else{
                                $metodo='Tarjeta de debito';
                                $totald=$totald+$item->total;

                            }
                            $datos.='<td>'.$metodo.'</td>';
                            $datos.='<td>'.$item->sucursal.'</td>';
                          $datos.='</tr>';
                          $total=$total+$item->total;
                    }
                    $datos.='</tbody>';
                $datos.='</table>';

                $datos.='<div class="row">';
                    $datos.='<div class="col-md-12">';
                                $datos.='<p><b>Total Efectivo:</b> $ '.$totale.'</p>';
                    $datos.='</div>';
                    $datos.='<div class="col-md-12">';
                                $datos.='<p><b>Total Tarjeta de Credito:</b> $'.$totalc.'</p>';
                    $datos.='</div>';
                    $datos.='<div class="col-md-12">';
                                $datos.='<p><b>Total Tarjeta de Debito:</b> $'.$totald.'</p>';
                    $datos.='</div>';
                    $datos.='<div class="col-md-12">';
                                $datos.='<p><b>Total:</b> $'.$total.'</p>';
                    $datos.='</div>';
                $datos.='</div>';

                break;
            case '2':
                $resultado=$this->ModeloReportes->getventascredito($fechaini,$fechafin,$this->sucursalId);
                $datos.='<div class="row">';
                    $datos.='<div class="col-md-12" >';
                                $datos.='<h2 style="text-align:center;">VENTAS A CREDITO</h2>';
                    $datos.='</div>';
                $datos.='</div>';
                $datos.='<table class="table table-striped jambo_table bulk_action" id="data-tables">';
                    $datos.='<thead>';
                      $datos.='<tr >';
                        $datos.='<th>#</th>';
                        $datos.='<th>Cajero</th>';
                        $datos.='<th>Cliente</th>';
                        $datos.='<th>Fecha</th>';
                        $datos.='<th>SubTotal</th>';
                        $datos.='<th>Descuento</th>';
                        $datos.='<th>Total</th>';
                        $datos.='<th>Metodo</th>';
                        $datos.='<th></th>';
                      $datos.='</tr>';
                    $datos.='</thead>';
                    $datos.='<tbody>';
                    foreach ($resultado->result() as $item){
                        $datos.='<tr >';
                            $datos.='<td>'.$item->ventaId.'</td>';
                            $datos.='<td>'.$item->cajero.'</td>';
                            $datos.='<td>'.$item->cliente.'</td>';
                            $datos.='<td>'.$item->reg.'</td>';
                            $datos.='<td>'.$item->subtotal.'</td>';
                            $datos.='<td>'.$item->descuento.'</td>';
                            $datos.='<td>'.$item->total.'</td>';
                            if ($item->pagado==1) {
                                $pagado='Pagado';
                            }else{
                                $pagado='';
                            }
                            $datos.='<td>'.$pagado.'</td>';
                            $datos.='<td>'.$item->sucursal.'</td>';
                          $datos.='</tr>';
                    }
                    $datos.='</tbody>';
                $datos.='</table>';

                $datos.='<div class="row">';
                    $datos.='<div class="col-md-12" >';
                                $datos.='<h2 style="text-align:center;">PAGOS CREDITOS</h2>';
                    $datos.='</div>';
                $datos.='</div>';


                $resultadop=$this->ModeloReportes->getpagoscreditos($fechaini,$fechafin,$this->sucursalId);
                $datos.='<table class="table table-striped jambo_table bulk_action" id="data-tables2">';
                    $datos.='<thead>';
                      $datos.='<tr >';
                        $datos.='<th>#</th>';
                        $datos.='<th>Venta</th>';
                        $datos.='<th>Cliente</th>';
                        $datos.='<th>Cajero</th>';
                        $datos.='<th>Pago</th>';
                        $datos.='<th>Fecha</th>';
                        $datos.='<th></th>';
                      $datos.='</tr>';
                    $datos.='</thead>';
                    $datos.='<tbody>';
                    $total=0;
                    foreach ($resultadop->result() as $item){
                            $datos.='<tr >';
                                $datos.='<td>'.$item->pagoId.'</td>';
                                $datos.='<td>'.$item->ventaId.'</td>';
                                $datos.='<td>'.$item->cliente.'</td>';
                                $datos.='<td>'.$item->cajero.'</td>';
                                $datos.='<td>'.$item->pago.'</td>';
                                $datos.='<td>'.$item->reg.'</td>';
                                $datos.='<td>'.$item->sucursal.'</td>';
                            $datos.='</tr>';
                            $total=$total+$item->pago;
                    }
                    $datos.='</tbody>';
                $datos.='</table>';
                $datos.='<div class="row">';
                    $datos.='<div class="col-md-12">';
                                $datos.='<p><b>Total:</b> $'.$total.'</p>';
                    $datos.='</div>';
                $datos.='</div>';

                break;
            case '3':
                $datos.='<div class="row">';
                    $datos.='<div class="col-md-12" >';
                                $datos.='<h2 style="text-align:center;">COMPRAS</h2>';
                    $datos.='</div>';
                $datos.='</div>';
                $datos.='<table class="table table-striped jambo_table bulk_action" id="data-tables">';
                    $datos.='<thead>';
                      $datos.='<tr >';
                        $datos.='<th>#</th>';
                        $datos.='<th>Proveedor</th>';
                        $datos.='<th>Personal</th>';
                        $datos.='<th>Monto</th>';
                        $datos.='<th>Fecha</th>';
                        $datos.='<th></th>';
                      $datos.='</tr>';
                    $datos.='</thead>';
                    $datos.='<tbody>';
                     $resultado=$this->ModeloReportes->getcompras($fechaini,$fechafin,$this->sucursalId);
                     $totalc=0;
                     foreach ($resultado->result() as $item){
                        $datos.='<tr >';
                            $datos.='<td>'.$item->compraId.'</td>';
                            $datos.='<td>'.$item->razon_social.'</td>';
                            $datos.='<td>'.$item->nombre.'</td>';
                            $datos.='<td>'.$item->monto_total.'</td>';
                            $datos.='<td>'.$item->reg.'</td>';
                            $datos.='<td>'.$item->sucursal.'</td>';
                          $datos.='</tr>';
                          $totalc=$totalc+$item->monto_total;

                     }
                     $datos.='</tbody>';
                $datos.='</table>';
                $datos.='<div class="row">';
                    $datos.='<div class="col-md-12">';
                                $datos.='<p><b>Total:</b> $'.$totalc.'</p>';
                    $datos.='</div>';
                $datos.='</div>';
                break;
            case '4':
                $resultado=$this->ModeloReportes->getproductos($fechaini,$fechafin,$this->sucursalId);
                $datos.='<div class="row">';
                    $datos.='<div class="col-md-12" >';
                                $datos.='<h2 style="text-align:center;">PRODUCTOS VENDIDOS</h2>';
                    $datos.='</div>';
                $datos.='</div>';
                $datos.='<table class="table table-striped jambo_table bulk_action" id="data-tables">';
                    $datos.='<thead>';
                      $datos.='<tr >';
                        $datos.='<th>Producto</th>';
                        $datos.='<th>Cantidad</th>';
                        $datos.='<th>Subtotal</th>';
                        $datos.='<th>Descuento</th>';
                        $datos.='<th>Total</th>';
                        $datos.='<th>Utilidad</th>';
                        $datos.='<th></th>';
                      $datos.='</tr>';
                    $datos.='</thead>';
                    $datos.='<tbody>';
                    foreach ($resultado->result() as $item){
                          $datos.='<tr >';
                            $datos.='<td>'.$item->producto.'</td>';
                            $datos.='<td>'.$item->cantidad.'</td>';
                            $datos.='<td>'.$item->subtotal.'</td>';
                            $datos.='<td>'.round($item->descuento,2).'</td>';
                            $datos.='<td>'.round($item->total,2).'</td>';
                            $datos.='<td>'.round($item->utilidad,2).'</td>';
                            $datos.='<td>'.$item->sucursal.'</td>';
                          $datos.='</tr>';

                    }
                    $datos.='</tbody>';
                $datos.='</table>';

                break;
            case '5':
                $resultado=$this->ModeloReportes->getproductosbajos($this->sucursalId);
                $datos.='<div class="row">';
                    $datos.='<div class="col-md-12" >';
                                $datos.='<h2 style="text-align:center;">PRODUCTOS DE BAJO INVENTARIO</h2>';
                    $datos.='</div>';
                $datos.='</div>';
                $datos.='<table class="table table-striped jambo_table bulk_action" id="data-tables">';
                    $datos.='<thead>';
                      $datos.='<tr >';
                        $datos.='<th>Sucursal</th>';
                        $datos.='<th>Producto</th>';
                        $datos.='<th>Existencia</th>';
                      $datos.='</tr>';
                    $datos.='</thead>';
                    $datos.='<tbody>';
                    foreach ($resultado->result() as $item){
                          $datos.='<tr >';
                            $datos.='<td>'.$item->sucursal.'</td>';
                            $datos.='<td>'.$item->nombre.'</td>';
                            $datos.='<td>'.$item->existencia.'</td>';
                           

                          $datos.='</tr>';

                    }
                    $datos.='</tbody>';
                $datos.='</table>';
                break;
            
            default:
                # code...
                break;
        }
        echo $datos;
    }
   

    

}