<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListadoVentasNormales extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->personal=$this->session->userdata('idpersonal');
            if ($this->perfilid==1) {
                $this->sucursalId=0;
            }
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,17);// 17 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
    }

	public function index(){
            $pages=10;
            if (isset($_GET['search'])) {
                $buscar=$_GET['search'];
            }else{
                $buscar='';
            }
            $data['buscar']=$buscar;
            $this->load->library('pagination');
            $config['base_url'] = base_url().'ListadoVentasNormales/view';
            $config['total_rows'] = $this->ModeloCatalogos->filastotal_ventasnormales($buscar,$this->sucursalId);
            $config['per_page'] = $pages;
            $this->pagination->initialize($config);
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["Ventas"] = $this->ModeloCatalogos->List_table_ventasnormales($pagex,$config['per_page'],$buscar,$this->sucursalId);
        //====================================   
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('listadoventas/normales',$data);
        $this->load->view('templates/footer');
        $this->load->view('listadoventas/jsnormales');
	}

    function cancelar(){
        $ventaId = $this->input->post('ventaId');
        $cancela_motivo = $this->input->post('cancela_motivo');
        $sumastock = $this->ModeloCatalogos->getventasdestalle($ventaId);
        foreach ($sumastock->result() as $item) {
            $cantidad = $item->cantidad;
            $idproducto = $item->productoid;
            $productos = $this->ModeloCatalogos->getproducto($idproducto);
            foreach ($productos->result() as $item) {
                $stock1 = $item->stock1; 
            }
            $suma = $cantidad + $stock1;
            $arraystock1 = array('stock1' => $suma);
            $result = $this->ModeloCatalogos->updateCatalogo('productos',$arraystock1,'productoid',$idproducto);
        }
        $data = array('cancelado'=>1,'cancela_personal'=> $this->personal,'cancela_motivo' => $cancela_motivo,'canceladoh'=> $this->fechahoy );
        $result = $this->ModeloCatalogos->updateCatalogo('ventas',$data,'ventaId',$ventaId);
    }
}