<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloReportes');
        date_default_timezone_set('America/Mexico_City');
        $this->anioactual = date('Y');
    }
	public function index(){
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $perfilid=$this->session->userdata('perfilid');
            $sucursalId=$this->session->userdata('sucursalId');
            $sucursalIdp=$sucursalId;
            $data['ventastotal']=$this->ModeloCatalogos->gettotaldeventas($perfilid,$sucursalId);
            $data['comprasy']=$this->ModeloCatalogos->gettotaldecompras($perfilid,$sucursalId);
            if ($perfilid==1) {
                $sucursalId=0;
            }
            $data['sucursalId']=$sucursalId;

        }
        /*
        $prowhere= array('activo' => 1,'reg>='=>'2019-04-21');
        $data['productosnew']=$this->ModeloCatalogos->getselectvalue1rowwheren('productos',$prowhere);*/
        $data['ventasnew']=$this->ModeloCatalogos->getultimasventas($sucursalId);
        $data['productosnew']=$this->ModeloCatalogos->getultimosproductos();
        $productosstock=$this->ModeloCatalogos->getproductosstocktotal();
        foreach ($productosstock->result() as $row) {
            $stock1=$row->stock1;
            $stock2=$row->stock2;
            $stock3=$row->stock3;
            $total=$row->total;
        }
        $data['productostotal'] =$total; 
        if ($perfilid==1) {
            $data['productostock']=$stock1+$stock2+$stock3;
        }else{
            if ($sucursalIdp==1) {
                $data['productostock'] =$stock1;
            }elseif ($sucursalIdp==2) {
                $data['productostock'] =$stock2;
            }else{
                $data['productostock'] =$stock3;
            }
        }
        $data['clientestotal']=$this->ModeloCatalogos->gettotalclientes();

        $data['anioactual']=$this->anioactual;
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('inicio',$data);
        $this->load->view('templates/footer');
        $this->load->view('jsinicio',$data);
        //==================================
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
	}
    

}