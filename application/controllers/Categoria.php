<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,2);// 2 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
    }
	public function index(){
        
        //====================================
            $pages=10;
            if (isset($_GET['search'])) {
                $buscar=$_GET['search'];
            }else{
                $buscar='';
            }
            $this->load->library('pagination');
            $config['base_url'] = base_url().'Categoria/view';
            $config['total_rows'] = $this->ModeloCatalogos->filastotal_categoria($buscar);
            $config['per_page'] = $pages;
            $this->pagination->initialize($config);
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["Categoria"] = $this->ModeloCatalogos->List_table_categoria($pagex,$config['per_page'],$buscar);
        //====================================
            $data["buscar"]=$buscar;




		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/categorialista',$data);
        $this->load->view('templates/footer');
        $this->load->view('config/categoriajs');
        
	}
    function add(){
        $data = $this->input->post();
        $id=$data['categoriaId'];
        unset($data['categoriaId']);
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo('categoria',$data,'categoriaId',$id);
        }else{
            //echo $data;
            $this->ModeloCatalogos->Insert('categoria',$data);
        }
    }
    public function eliminar(){
        $id = $this->input->post('categoriaId');
        $data = array('activo' => 0);
        $this->ModeloCatalogos->updateCatalogo('categoria',$data,'categoriaId',$id);
    }
    

}