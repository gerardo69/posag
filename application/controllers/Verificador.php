<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verificador extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        //$this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,14);// 14 es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
    }
	public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('verificador/verificador');
        $this->load->view('templates/footer');
        $this->load->view('verificador/verificadorjs');
    }
    public function verifica(){
        $codigo = $this->input->post('codigo');
        $result = $this->ModeloCatalogos->List_table_verificar($codigo,$this->sucursalId);
    
        $html = '';
            $html .= '<table class="table table-striped jambo_table bulk_action" id="data-tables">';
            $html .= '<thead>';
              $html .= '<tr>';
                $html .= '<th></th>';
                $html .= '<th>Código</th>';
                $html .= '<th>Nombre</th>';
                $html .= '<th>Precio venta</th>';
              $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
           
              foreach ($result->result() as $item) {
                $html .="<tr>";
                  $html .= "<td>";
                          if ($item->img!='') {
                            $img='public/images/productost/'.$item->img;
                          }
                  $html .= "<img src='".$img."' class='imgpro'>";
                  $html .= "</td>";
                  $html .= "<td>".$item->codigo."</td>";
                  $html .= "<td>".$item->nombre."</td>";
                  $html .= "<td>".$item->precio_venta."</td>";
                $html .= "</tr>";
                }
            $html .= "</tbody>";
          $html .= "</table>";
        
        $html .= '';
        
        echo $html;

    }


}