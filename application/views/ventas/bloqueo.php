<!-- page content -->
<div class="right_col" role="main">

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="dashboard_graph">

        <div class="row x_title">
          <div class="col-md-6">
            <h3>Notificación<small></small></h3>
          </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
          <!--------//////////////-------->
          
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Vencimiento de licencia.<small></small></h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="bs-example" data-example-id="simple-jumbotron">
                    <div class="jumbotron">
                      <h1>Mensaje</h1>
                      <p>El servicio de su licencia se ha agotado. Le sugerimos contactar al administrador para que puedar adquirir de nuevo el servicio.</p>
                    </div>
                  </div>

                </div>
              </div>
            </div> 
          </div>
          <!--------//////////////-------->
        </div>
        <div class="clearfix"></div>
      </div>
    </div>

  </div>
  <br />

</div>
