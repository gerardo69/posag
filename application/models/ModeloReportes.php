<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloReportes extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->year = date('Y');
    }
    function getproductos($finicio,$ffin,$sucursalId){
        if ($sucursalId==0) {
            $where='';
        }else{
            $where=' and v.sucursalid='.$sucursalId;
        }
        $strq="SELECT 
                producto, 
                sum(cantidad) as cantidad,
                sum(subtotal) as subtotal,
                sum(descuento) as descuento,
                sum(total) as total,
                sum(total-totalcompra) as utilidad,
                sucursal,
                sucursalid 
                from(
                    SELECT v.ndescuento,vd.productoid,
                            pro.nombre as producto,
                            vd.cantidad,
                            (vd.cantidad*vd.precio) as subtotal,
                            (subtotal*v.ndescuento)as descuento,
                            (subtotal-descuento) as total, 
                            vd.precioc,(vd.cantidad*vd.precioc) as totalcompra,
                            suc.sucursal,
                            v.sucursalid
                    FROM ventas as v 
                    inner join venta_detalle as vd on vd.ventaId=v.ventaId
                    inner JOIN productos as pro on pro.productoid=vd.productoid
                    inner join sucursales as suc on suc.sucursalid=v.sucursalid
                    WHERE v.pagado=1 $where and v.cancelado=0 AND v.reg BETWEEN '$finicio 00:00:00' AND '$ffin 23:59:59'
                ) as productos
                GROUP by productoid,sucursalid";
                //log_message('error', $strq);
        $resp=$this->db->query($strq);
        return $resp;
    }
    function getventascontado($finicio,$ffin,$sucursalId){
        if ($sucursalId==0) {
            $where='';
        }else{
            $where=' and v.sucursalid='.$sucursalId;
        }
        $strq="SELECT v.ventaId, 
                        p.nombre as cajero,
                        c.Nombre as cliente, 
                        v.reg,
                        v.subtotal,
                        v.descuento,
                        v.total,
                        v.metodo,
                        suc.sucursal
                from ventas as v
                inner JOIN personal as p on p.personalId=v.personalId
                inner JOIN clientes as c on c.ClientesId=v.ClientesId
                inner join sucursales as suc on suc.sucursalid=v.sucursalid
                WHERE v.tipopago=1 $where AND v.reg BETWEEN '$finicio 00:00:00' AND '$ffin 23:59:59' ";
        $resp=$this->db->query($strq);
        return $resp;
    }
    function getventascredito($finicio,$ffin,$sucursalId){
        if ($sucursalId==0) {
            $where='';
        }else{
            $where=' and v.sucursalid='.$sucursalId;
        }
        $strq="SELECT v.ventaId, 
                        p.nombre as cajero,
                        c.Nombre as cliente, 
                        v.reg,
                        v.subtotal,
                        v.descuento,
                        v.total,
                        v.pagado,
                        suc.sucursal
                from ventas as v
                inner JOIN personal as p on p.personalId=v.personalId
                inner JOIN clientes as c on c.ClientesId=v.ClientesId
                inner join sucursales as suc on suc.sucursalid=v.sucursalid
                WHERE v.tipopago=2 $where AND v.reg BETWEEN '$finicio 00:00:00' AND '$ffin 23:59:59' ";
        $resp=$this->db->query($strq);
        return $resp;
    }
    function getpagoscreditos($finicio,$ffin,$sucursalId){
        if ($sucursalId==0) {
            $where='';
        }else{
            $where=' v.sucursalid='.$sucursalId.' and ';
        }
        $strq="SELECT p.pagoId, p.ventaId,c.Nombre as cliente, pe.nombre as cajero, p.pago, p.reg,suc.sucursal
                FROM pagos_credito as p
                inner join personal as pe on pe.personalId=p.personalId
                inner JOIN ventas as v on v.ventaId=p.ventaId
                inner JOIN clientes as c on c.ClientesId=v.ClientesId
                inner join sucursales as suc on suc.sucursalid=v.sucursalid
                WHERE $where p.reg BETWEEN '$finicio 00:00:00' AND '$ffin 23:59:59' ";
        $resp=$this->db->query($strq);
        return $resp;
    }
    function getcompras($finicio,$ffin,$sucursalId){
        if ($sucursalId==0) {
            $where='';
        }else{
            $where=' com.sucursalid='.$sucursalId.' and ';
        }
        $strq="SELECT com.compraId,pro.razon_social,per.nombre,com.monto_total,com.reg,suc.sucursal
                FROM compras as com
                inner JOIN proveedores as pro on pro.id_proveedor=com.id_proveedor
                inner join personal as per on per.personalId=com.personalId
                inner join sucursales as suc on suc.sucursalid=com.sucursalid
                WHERE $where com.reg BETWEEN '$finicio 00:00:00' AND '$ffin 23:59:59' ";
        $resp=$this->db->query($strq);
        return $resp;
    }
    function gettotalcomprasmes($mes,$sucursal){
        $primerdiadelmes=$this->year.'-'.$mes.'-01';
        $fecha = new DateTime($primerdiadelmes);
        $fecha->modify('last day of this month');
        $ultimodiadelmes=$fecha->format('Y-m-d');
        if ($sucursal==0) {
            $wheres='';
            $wjoin='';
        }else{
            $wjoin='inner join personal as per on per.personalId=com.personalId';
            $wheres=' per.sucursalId='.$sucursal.' AND ';
        }
        $strq="SELECT sum(com.monto_total) as total
                FROM compras as com
                $wjoin
                WHERE $wheres com.reg BETWEEN '$primerdiadelmes 00:00:00' AND '$ultimodiadelmes 23:59:59' ";
                
        $resp=$this->db->query($strq);
        //var_dump($strq);
        
        $total=0;
        foreach ($resp->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    function gettotalventassmes($mes,$sucursal){
        $primerdiadelmes=$this->year.'-'.$mes.'-01';
        $fecha = new DateTime($primerdiadelmes);
        $fecha->modify('last day of this month');
        $ultimodiadelmes=$fecha->format('Y-m-d');
        if ($sucursal==0) {
            $wheres='';
            $wjoin='';
        }else{
            $wjoin='inner join personal as per on per.personalId=v.personalId';
            $wheres=' per.sucursalId='.$sucursal.' AND ';
        }
        $strq="SELECT sum(v.total) as total
                FROM ventas as v
                $wjoin
                WHERE $wheres v.pagado=1 and v.cancelado=0 and v.reg BETWEEN '$primerdiadelmes 00:00:00' AND '$ultimodiadelmes 23:59:59' ";
                
        $resp=$this->db->query($strq);
        //var_dump($strq);
        
        $total=0;
        foreach ($resp->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    function getproductosbajos($sucursal){
        if ($sucursal==0) {
            $where='';
        }else{
            $where=' and sucu.idsucursal='.$sucursal;
        }
        $strq="SELECT sucu.sucursal,pro.nombre, pros.existencia
                FROM productos_sucursales as pros
                inner join sucursales as sucu on sucu.sucursalid=pros.idsucursal
                inner join productos as pro on pro.productoid=pros.idproducto
                WHERE 
                pro.activo=1 and 
                sucu.activo=1 and 
                pros.existencia<10
                $where";
                //log_message('error', $strq);
        $resp=$this->db->query($strq);
        return $resp;
    }
    function getsucursalactiva($sucu){
        $strq="SELECT * from sucursales where sucursalid= $sucu and activo=1";
        $resp=$this->db->query($strq);
        $activo=0;
        foreach ($resp->result() as $row) {
            $activo=$row->activo;;
        }
        return $activo;
    }
    
    
}
