<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function updateCatalogo($Tabla,$data,$idname,$id){
        $this->db->set($data);
        $this->db->where($idname, $id);
        $this->db->update($Tabla);
        return $id;
    }
    public function deleteCatalogo($table,$idname,$id){
        $this->db->where($idname, $id);
        $this->db->delete($table);
    }
    function updatestock($Tabla,$value,$masmeno,$value2,$idname,$id){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname=$id ";
        $query = $this->db->query($strq);
        return $id;
    }
    function updatestock2($Tabla,$value,$masmeno,$value2,$idname,$id,$idname2,$id2){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname=$id and $idname=$id";
        $query = $this->db->query($strq);
        return $id;
    }
    function getselectvalue1rowwhere($table,$colw,$valw){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colw,$valw);
        $query=$this->db->get(); 
        return $query;
    }
    function getselectvalue1rowwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function getultimosproductos(){
        $strq = "SELECT * FROM productos where activo=1 ORDER BY productoid DESC LIMIT 5";
        $query = $this->db->query($strq);
        return $query;
    }
    function getproductosstocktotal(){
        $strq = "SELECT 
                        sum(stock1) as stock1,
                        sum(stock2) as stock2,
                        sum(stock3) as stock3,
                        COUNT(*) as total 
                        from productos 
                        where activo=1";
        $query = $this->db->query($strq);
        return $query;
    }
    function getultimasventas($sucursal){
        if ($sucursal==0) {
            $winner='';
            $where='';
        }else{
            $winner=' inner JOIN personal as per on per.personalId=v.personalId ';
            $where=' and per.sucursalId='.$sucursal;
        }

        $strq = "SELECT v.ventaId,cli.Nombre ,v.total, v.reg 
                FROM ventas as v 
                inner JOIN clientes as cli on cli.ClientesId=v.ClientesId
                $winner
                WHERE v.pagado=1 $where
                ORDER BY v.ventaId DESC LIMIT 3";
        $query = $this->db->query($strq);
        return $query;
    }
    function gettotaldeventas($perfil,$sucursal){
        $year=date('Y');
        if ($perfil>1) {
            $where ='';
            $innerjoin='';
        }else{
            $innerjoin=' inner join personal as pe on pe.personalId=ve.personalId ';
            $where =' and pe.sucursalId='.$sucursal;
        }
        $strq = "SELECT sum(ve.total) as total 
                from ventas as ve 
                $innerjoin
                WHERE ve.pagado=1 and ve.cancelado=0 AND ve.reg>='$year-01-01 00:00:00' $where";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    function gettotaldecompras($perfil,$sucursal){
        $year=date('Y');
        if ($perfil>1) {
            $where ='';
            $innerjoin='';
        }else{
            $innerjoin=' inner join personal as pe on pe.personalId=com.personalId ';
            $where =' and pe.sucursalId='.$sucursal;
        }
        $strq = "SELECT sum(com.monto_total) as total,count(*) as reg 
                from compras as com 
                $innerjoin
                WHERE com.reg>='$year-01-01 00:00:00' $where";
        $query = $this->db->query($strq);
        $total=0;
        $reg=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
            $reg=$row->reg;
        }
        $compras = array('total' => $total,'reg'=>$reg );
        return $compras; 
    }
    function gettotalclientes(){
        $strq = "SELECT COUNT(*) as total FROM clientes WHERE activo=1";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    public function getselectwherelike2($tables,$where,$likec1,$likev1,$likec2,$likev2){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $this->db->like($likec1,$likev1, 'both'); 
        $this->db->or_like($likec2,$likev2, 'both');  
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    public function getselectwherelike1($tables,$where,$likec1,$likev1){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $this->db->like($likec1,$likev1, 'both'); 
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    function tickeall(){
        $strq = "SELECT * FROM ticket";
        $query = $this->db->query($strq);
        return $query;
    }
    function getviewpermiso($perfil,$modulo){
        $strq = "SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=$perfil AND MenusubId=$modulo";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    function codebar($id){
        $strq = "SELECT codigo FROM `ticket_bar` WHERE barId=$id";
        $query = $this->db->query($strq);
        $codigo=0;
        foreach ($query->result() as $row) {
            $codigo=$row->codigo;
        }
        return $codigo; 
    }
    //================ listado personal ===========================
        function filastotal_personal($buscar,$sucursal){
            if ($sucursal==0) {
                $wsucursal='';
            }else{
                $wsucursal=' and sucursalId='.$sucursal;
            }
            if ($buscar=='') {
                $where='activo=1 and tipo=1 '.$wsucursal;
            }else{
                $whereb=" activo=1 and tipo=1 ".$wsucursal;
                $where= $whereb." and nombre like '%".$buscar."%' or ";
                $where.= $whereb." and correo like '%".$buscar."%' ";
            }
            $strq = "SELECT COUNT(*) as total 
                    FROM personal 
                    where $where";
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
            $total =$row->total;
            } 
            return $total;
        }
        function List_table_personal($por_pagina,$segmento,$buscar,$sucursal){
            if ($sucursal==0) {
                $wsucursal='';
            }else{
                $wsucursal=' and sucursalId='.$sucursal;
            }
            if ($segmento!='') {
                $segmento=','.$segmento;
            }else{
                $segmento='';
            }
            if ($buscar=='') {
                $where='activo=1 and tipo=1 '.$wsucursal;
            }else{
                $whereb=" activo=1 and tipo=1 ".$wsucursal;
                $where= $whereb." and nombre like '%".$buscar."%' or ";
                $where.= $whereb." and correo like '%".$buscar."%'";
            }
            $strq="SELECT * FROM personal WHERE $where LIMIT $por_pagina $segmento;";
            $resp=$this->db->query($strq);
            return $resp;
        }
    //================ listado personal fin ===========================
    //================ listado categoria ===========================
        function filastotal_categoria($buscar){
            if ($buscar=='') {
                $where='activo=1';
            }else{
                $whereb=" activo=1";
                $where= $whereb." and categoria like '%".$buscar."%'";
            }
            $strq = "SELECT COUNT(*) as total 
                    FROM categoria 
                    where $where";
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
            $total =$row->total;
            } 
            return $total;
        }
        function List_table_categoria($por_pagina,$segmento,$buscar){
            if ($segmento!='') {
                $segmento=','.$segmento;
            }else{
                $segmento='';
            }
            if ($buscar=='') {
                $where='cat.activo=1';
            }else{
                $whereb=" cat.activo=1";
                $where= $whereb." and cat.categoria like '%".$buscar."%' ";
            }
            $strq="SELECT cat.categoriaId,cat.categoria,cat.activo,cat.reg,
                    (
                        SELECT count(*) as total from productos as pro where pro.categoria=cat.categoriaId
                    ) as total
                    FROM categoria as cat 
                    WHERE $where LIMIT $por_pagina $segmento;";
            $resp=$this->db->query($strq);
            return $resp;
        }
    //================ listado personal fin ===========================
    //================ listado productos ===========================
        function filastotal_productos(){
            $strq = "SELECT COUNT(*) as total 
                    FROM  productos as pro
                    where pro.activo=1";
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
            $total =$row->total;
            } 
            return $total;
        }/*
        function List_table_productos($por_pagina,$segmento,$buscar){
            if ($segmento!='') {
                $segmento=','.$segmento;
            }else{
                $segmento='';
            }
            if ($buscar=='') {
                $where=' pro.activo=1 ';
            }else{
                $whereb=" pro.activo=1 ";
                $where= $whereb." and pro.codigo like '%".$buscar."%' or ";
                $where.= $whereb." and pro.nombre like '%".$buscar."%' or ";
                $where.= $whereb." and cat.categoria like '%".$buscar."%' or ";
                $where.= $whereb." and pro.precioventa like '%".$buscar."%' ";
            }
            $strq="SELECT pro.productoid,pro.img,pro.codigo,pro.nombre,pro.descripcion,cat.categoria,pro.stock1,pro.stock2,pro.stock3,pro.precioventa
                    FROM productos as pro 
                    inner join categoria as cat on cat.categoriaId=pro.categoria
                    WHERE $where order by pro.productoid DESC LIMIT $por_pagina $segmento;";
            $resp=$this->db->query($strq);
            return $resp;
        }*/
        function List_table_productos_asi($params){
            $sucursal=$params['sucu'];
            $columns = array( 
                0=>'pro.productoid',
                1=>'pro.img',
                2=>'pro.codigo',
                3=>'pro.nombre',
                4=>'pro.descripcion',
                5=>'cat.categoria',
                6=>'pros.existencia',
                7=>'pros.precio_venta',
                 
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('productos pro');
            $this->db->join('categoria cat', 'cat.categoriaId=pro.categoria');
            $this->db->join('productos_sucursales pros', 'pros.idproducto=pro.productoid','left');
            $where = array(
                'pro.activo'=>1,
                'pros.idsucursal'=>$sucursal
            );
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }

    //================ listado personal fin ===========================
    //================ listado clientes ===========================
        function filastotal_clientes($buscar){
            if ($buscar=='') {
                $where='activo=1';
            }else{
                $whereb=" activo=1 ";
                $where= $whereb." and Nombre like '%".$buscar."%' or ";
                $where.= $whereb." and Domicilio like '%".$buscar."%' or ";
                $where.= $whereb." and Correo like '%".$buscar."%' or ";
                $where.= $whereb." and nombrec like '%".$buscar."%' or ";
                $where.= $whereb." and correoc like '%".$buscar."%' or ";
                $where.= $whereb." and telefonoc like '%".$buscar."%' ";
            }
            $strq = "SELECT COUNT(*) as total 
                    FROM clientes 
                    where $where";
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
            $total =$row->total;
            } 
            return $total;
        }
        function List_table_clientes($por_pagina,$segmento,$buscar){
            if ($segmento!='') {
                $segmento=','.$segmento;
            }else{
                $segmento='';
            }
            if ($buscar=='') {
                $where='activo=1';
            }else{
                $whereb=" activo=1 ";
                $where= $whereb." and Nombre like '%".$buscar."%' or ";
                $where.= $whereb." and Domicilio like '%".$buscar."%' or ";
                $where.= $whereb." and Correo like '%".$buscar."%' or ";
                $where.= $whereb." and nombrec like '%".$buscar."%' or ";
                $where.= $whereb." and correoc like '%".$buscar."%' or ";
                $where.= $whereb." and telefonoc like '%".$buscar."%' ";
            }
            $strq="SELECT * FROM clientes WHERE $where LIMIT $por_pagina $segmento;";
            $resp=$this->db->query($strq);
            return $resp;
        }
    //================ listado clientes fin ===========================
    //================ listado proveedores ===========================    
        function filastotal_proveedores($buscar){
                if ($buscar=='') {
                    $where='activo=1';
                }else{
                    $whereb=" activo=1 ";
                    $where= $whereb." and razon_social like '%".$buscar."%' or ";
                    $where.= $whereb." and domicilio like '%".$buscar."%' or ";
                    $where.= $whereb." and ciudad like '%".$buscar."%' or ";
                    $where.= $whereb." and telefono_local like '%".$buscar."%' or ";
                    $where.= $whereb." and telefono_celular like '%".$buscar."%' or ";
                    $where.= $whereb." and email_contacto like '%".$buscar."%' or ";
                    $where.= $whereb." and rfc like '%".$buscar."%' ";
                }
                $strq = "SELECT COUNT(*) as total 
                        FROM proveedores 
                        where $where ";
                $query = $this->db->query($strq);
                $this->db->close();
                foreach ($query->result() as $row) {
                $total =$row->total;
                } 
                return $total;
            }
        function List_table_proveedores($por_pagina,$segmento,$buscar){
                if ($segmento!='') {
                    $segmento=','.$segmento;
                }else{
                    $segmento='';
                }
                if ($buscar=='') {
                    $where='activo=1';
                }else{
                    $whereb=" activo=1 ";
                    $where= $whereb." and razon_social like '%".$buscar."%' or ";
                    $where.= $whereb." and domicilio like '%".$buscar."%' or ";
                    $where.= $whereb." and ciudad like '%".$buscar."%' or ";
                    $where.= $whereb." and telefono_local like '%".$buscar."%' or ";
                    $where.= $whereb." and telefono_celular like '%".$buscar."%' or ";
                    $where.= $whereb." and email_contacto like '%".$buscar."%' or ";
                    $where.= $whereb." and rfc like '%".$buscar."%' ";
                }
                $strq="SELECT * FROM proveedores WHERE $where ORDER BY id_proveedor DESC LIMIT $por_pagina $segmento;";
                $resp=$this->db->query($strq);
                return $resp;
            }
    //================ listado proveedores fin ===========================    
    //================ Verifcar Producto ===================
        function List_table_verificar($buscar,$sucursal){
            $strq="SELECT pro.productoid,pro.img,pro.codigo,pro.nombre,pros.precio_venta
                    FROM productos as pro
                    inner join productos_sucursales as pros on pros.idproducto=pro.productoid
                    WHERE 
                    pro.activo=1 AND 
                    pros.idsucursal=$sucursal and 
                    pro.codigo ='$buscar'";
            $resp=$this->db->query($strq);
            return $resp;
        }
    //================ Verificar Producto fin ===========    
    //================ Gastos ===========
        function filastotal_gastos($buscar,$sucursal){
            if ($sucursal==0) {
                $wsucursal='';
            }else{
                $wsucursal=' and p.sucursalId='.$sucursal.'';
            }

            if ($buscar=='') {
                $where=' activod=1  '.$wsucursal;
            }else{
                $where0=' activod=1 '.$wsucursal ;
                $where= $where0." and g.gastosid like '%".$buscar."%' or ";
                $where.=$where0." and g.fecha like '%".$buscar."%' or ";
                $where.=$where0." and g.cantidad like '%".$buscar."%' or ";
                $where.=$where0." and g.concepto like '%".$buscar."%' or ";
                $where.=$where0." and p.nombre like '%".$buscar."%' or ";
                $where.=$where0." and s.sucursal like '%".$buscar."%' ";
            }
            $strq = "SELECT COUNT(*) as total 
                     FROM gastos as g 
                     inner join personal as p on p.personalId = g.personalId 
                     inner join sucursales as s on s.sucursalid = g.sucursal
                     where $where";
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
            $total =$row->total;
            } 
            return $total;
        }
        function List_table_gastos($por_pagina,$segmento,$buscar,$sucursal){
            if ($sucursal==0) {
                $wsucursal='';
            }else{
                $wsucursal=' and p.sucursalId='.$sucursal.'';
            }
            if ($segmento!='') {
                    $segmento=','.$segmento;
                }else{
                    $segmento='';
                }

            if ($buscar=='') {
                $where=' activod=1  '.$wsucursal;
            }else{
                $where0=' activod=1 '.$wsucursal ;
                $where= $where0." and g.gastosid like '%".$buscar."%' or ";
                $where.=$where0." and g.fecha like '%".$buscar."%' or ";
                $where.=$where0." and g.cantidad like '%".$buscar."%' or ";
                $where.=$where0." and g.concepto like '%".$buscar."%' or ";
                $where.=$where0." and p.nombre like '%".$buscar."%' or ";
                $where.=$where0." and s.sucursal like '%".$buscar."%' ";
            }
            $strq="SELECT g.gastosid,g.fecha,g.cantidad,g.concepto,p.nombre,s.sucursal,g.activo,pc.nombre as personalc,g.motivo_cancela,g.reg_cancela
                    FROM gastos as g 
                    inner join personal as p on p.personalId = g.personalId 
                    inner join sucursales as s on s.sucursalid = g.sucursal 
                    left join personal as pc on pc.personalId = g.personalId_cancela 
                    where  $where 
                    LIMIT $por_pagina $segmento;";
            $resp=$this->db->query($strq);
            return $resp;
        }
    //================ Gastos fin ===========        
    //================ Sucursal ===========
        function filastotal_sucursal($buscar){
            if ($buscar=='') {
                $where='activo=1';
            }else{
                $where=" sucursalid like '%".$buscar."%' or ";
                $where.=" sucursal like '%".$buscar."%' or ";
                $where.=" direccion like '%".$buscar."%' or ";
                $where.=" telefono like '%".$buscar."%'";
            }
            $strq = "SELECT COUNT(*) as total FROM sucursales WHERE $where";
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
            $total =$row->total;
            } 
            return $total;
        }
        function List_table_sucursal($por_pagina,$segmento,$buscar){
            if ($segmento!='') {
                $segmento=','.$segmento;
            }else{
                $segmento='';
            }
            if ($buscar=='') {
                $where='activo=1';
            }else{
                $where=" sucursalid like '%".$buscar."%' or ";
                $where.=" sucursal like '%".$buscar."%' or ";
                $where.=" direccion like '%".$buscar."%' or ";
                $where.=" telefono like '%".$buscar."%'";
            }
            $strq="SELECT * FROM sucursales WHERE $where LIMIT $por_pagina $segmento;";
            $resp=$this->db->query($strq);
            return $resp;
        }
    //================ Sucursal fin ===========  
    //================ Listado Ventas normales===========
        function filastotal_ventasnormales($buscar,$sucursal){
            if ($sucursal==0) {
                $wsucursal='';
            }else{
                $wsucursal=' and v.sucursalid='.$sucursal.' ';
            }
            if ($buscar=='') {
                $where='tipopago=1 '.$wsucursal;
            }else{
                $whereb="tipopago=1 ".$wsucursal;
                $where=$whereb."and v.ventaId like '%".$buscar."%' or ";
                $where.=$whereb."and c.Nombre like '%".$buscar."%' or ";
                $where.=$whereb."and v.subtotal like '%".$buscar."%' or ";
                $where.=$whereb."and v.ndescuento like '%".$buscar."%' or ";
                $where.=$whereb."and v.descuento like '%".$buscar."%' or ";
                $where.=$whereb."and v.total like '%".$buscar."%' ";
            }
            $strq = "SELECT COUNT(*) as total 
                     FROM ventas AS v 
                     LEFT JOIN clientes as c ON c.ClientesId = v.ClientesId 
                     left join personal as pc on pc.personalId = v.personalId 
                     WHERE $where";
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
            $total =$row->total;
            } 
            return $total;
        }
        
        function List_table_ventasnormales($por_pagina,$segmento,$buscar,$sucursal){
            if ($segmento!='') {
                $segmento=','.$segmento;
            }else{
                $segmento='';
            }
            if ($sucursal==0) {
                $wsucursal='';
            }else{
                $wsucursal=' and v.sucursalid='.$sucursal.'';
            }
            if ($buscar=='') {
                $where='tipopago=1 '.$wsucursal;
            }else{
                $whereb="tipopago=1 ".$wsucursal;
                $where=$whereb." and v.ventaId like '%".$buscar."%' or ";
                $where.=$whereb." and c.Nombre like '%".$buscar."%' or ";
                $where.=$whereb." and v.subtotal like '%".$buscar."%' or ";
                $where.=$whereb." and v.ndescuento like '%".$buscar."%' or ";
                $where.=$whereb." and v.descuento like '%".$buscar."%' or ";
                $where.=$whereb." and v.total like '%".$buscar."%' ";
            }
            $strq="SELECT v.ventaId, c.Nombre as cliente, v.subtotal, v.ndescuento, v.descuento, v.total, v.reg, v.cancelado, pc.nombre, v.cancela_motivo, v.canceladoh FROM ventas AS v 
                    LEFT JOIN clientes as c ON c.ClientesId = v.ClientesId 
                    left join personal as pc on pc.personalId = v.personalId
                    WHERE $where ORDER BY v.reg DESC LIMIT $por_pagina $segmento  ;";
            $resp=$this->db->query($strq);
            return $resp;
        }    
    //================ Listado Ventas normales fin ===========  
    //================ Listado Ventas Credito =============
        function filastotal_ventascredito($buscar,$sucursal){
            if ($sucursal==0) {
                $wsucursal='';
            }else{
                $wsucursal=' and v.sucursalid='.$sucursal.' ';
            }
            if ($buscar=='') {
                $where='tipopago=2 '.$wsucursal;
            }else{
                $whereb="tipopago=2 ".$wsucursal;
                $where=$whereb."and v.ventaId like '%".$buscar."%' or ";
                $where.=$whereb."and c.Nombre like '%".$buscar."%' or ";
                $where.=$whereb."and v.subtotal like '%".$buscar."%' or ";
                $where.=$whereb."and v.ndescuento like '%".$buscar."%' or ";
                $where.=$whereb."and v.descuento like '%".$buscar."%' or ";
                $where.=$whereb."and v.total like '%".$buscar."%' ";
            }
            $strq = "SELECT COUNT(*) as total FROM ventas AS v 
                     LEFT JOIN clientes as c ON c.ClientesId = v.ClientesId 
                     left join personal as pc on pc.personalId = v.personalId
                     WHERE $where";
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
            $total =$row->total;
            } 
            return $total;
        }
        function List_table_ventascredito($por_pagina,$segmento,$buscar,$sucursal){
            if ($sucursal==0) {
                $wsucursal='';
            }else{
                $wsucursal=' and v.sucursalid='.$sucursal.' ';
            }
            if ($segmento!='') {
                $segmento=','.$segmento;
            }else{
                $segmento='';
            }
            if ($buscar=='') {
                $where='tipopago=2'.$wsucursal;
            }else{
                $whereb="tipopago=2 ".$wsucursal;
                $where=$whereb."and v.ventaId like '%".$buscar."%' or ";
                $where.=$whereb."and c.Nombre like '%".$buscar."%' or ";
                $where.=$whereb."and v.subtotal like '%".$buscar."%' or ";
                $where.=$whereb."and v.ndescuento like '%".$buscar."%' or ";
                $where.=$whereb."and v.descuento like '%".$buscar."%' or ";
                $where.=$whereb."and v.total like '%".$buscar."%' or ";
                $where.=$whereb."and v.fechavencimiento like '%".$buscar."%' ";
            }
            $strq="SELECT v.ventaId, c.Nombre as cliente, v.subtotal, v.ndescuento, v.descuento, v.total, v.fechavencimiento, v.cancelado, v.pagado, p.nombre, v.cancela_motivo, v.canceladoh  FROM ventas AS v
                LEFT JOIN clientes AS c ON c.ClientesId = v.ClientesId 
                LEFT JOIN personal AS p on p.personalId = v.cancela_personal  WHERE $where ORDER BY v.reg DESC LIMIT $por_pagina $segmento;";
            $resp=$this->db->query($strq);
            return $resp;
        }
    //================ Listado Ventas Credito fin ===========      
    //================ Listado pagos ===========
        function List_table_pagos($buscar){
            $strq="SELECT  pagoId, pago, reg
                   FROM pagos_credito
                   WHERE ventaId ='$buscar'";
            $resp=$this->db->query($strq);
            return $resp;
        }
  
        function verificarcantidad($id){
            $strq="SELECT total FROM  ventas WHERE ventaId ='$id'";
            $resp=$this->db->query($strq);
            $total=0;
            foreach ($resp->result() as $row) {
                $total =$row->total;
                }
            return $total;
        }
        function verificarpago($buscar){
            $strq="SELECT SUM(p.pago) as suma, v.total FROM pagos_credito AS p
                   LEFT JOIN ventas as v ON  v.ventaId = p.ventaId
                   WHERE p.ventaId = '$buscar'";
            $resp=$this->db->query($strq);
            return $resp;
        }

    //================ Listado pagos fin =========== 
    //================ Listado compras ===========
        function filastotal_compras($buscar,$sucursal){
                if ($sucursal==0) {
                    $wsucursal='';
                }else{
                    $wsucursal=' and co.sucursalid='.$sucursal.' ';
                }
                if ($buscar=='') {
                    $where='co.activo=1 '.$wsucursal;
                }else{
                    $whereb="co.activo=1 ".$wsucursal;
                    $where=$whereb." and co.compraId like '%".$buscar."%' or ";
                    $where.=$whereb." and co.monto_total like '%".$buscar."%' or ";
                    $where.=$whereb." and co.reg like '%".$buscar."%' or ";
                    $where.=$whereb." and pr.razon_social like '%".$buscar."%' or ";
                    $where.=$whereb." and per.nombre like '%".$buscar."%'  ";
                }
                $strq = "SELECT COUNT(*) as total 
                        FROM compras as co
                        inner JOIN proveedores as pr on pr.id_proveedor=co.id_proveedor
                        inner JOIN personal as per on per.personalId=co.personalId 
                        where $where";
                $query = $this->db->query($strq);
                $this->db->close();
                foreach ($query->result() as $row) {
                $total =$row->total;
                } 
                return $total;
            }
        function List_table_compras($por_pagina,$segmento,$buscar,$sucursal){
                if ($sucursal==0) {
                    $wsucursal='';
                }else{
                    $wsucursal=' and co.sucursalid='.$sucursal.' ';
                }
                if ($segmento!='') {
                    $segmento=','.$segmento;
                }else{
                    $segmento='';
                }
                if ($buscar=='') {
                    $where='co.activo=1 '.$wsucursal;
                }else{
                    $whereb="co.activo=1".$wsucursal;
                    $where=$whereb." and co.compraId like '%".$buscar."%' or ";
                    $where.=$whereb." and co.monto_total like '%".$buscar."%' or ";
                    $where.=$whereb." and co.reg like '%".$buscar."%' or ";
                    $where.=$whereb." and pr.razon_social like '%".$buscar."%' or ";
                    $where.=$whereb." and per.nombre like '%".$buscar."%'  ";
                }
                $strq="SELECT co.compraId,co.monto_total,co.reg,pr.razon_social,per.nombre
                        FROM compras as co
                        inner JOIN proveedores as pr on pr.id_proveedor=co.id_proveedor
                        inner JOIN personal as per on per.personalId=co.personalId 
                        where $where LIMIT $por_pagina $segmento;";
                $resp=$this->db->query($strq);
                return $resp;
            }
    //================ Listado compras Fin===========
    //================ Modificacion del stoct1 ===========
    function getventasdestalle($buscar){
            $strq="SELECT vd.cantidad, p.nombre ,vd.productoid FROM `ventas` AS v 
                    INNER JOIN venta_detalle AS vd ON vd.ventaId = v.ventaId
                    INNER JOIN productos AS p ON p.productoid = vd.productoid
                   WHERE v.ventaId ='$buscar'";
            $resp=$this->db->query($strq);
            return $resp;
            }
    function getproducto($buscar){
            $strq="SELECT productoid, stock1, productoid  FROM productos
                   WHERE productoid ='$buscar'";
            $resp=$this->db->query($strq);
            return $resp;
            }        
    //================ Modificacion del stoct1 Fin ===========    
    function getbackup(){
        $strq="SELECT bac.backupid,bac.name,bac.reg,per.nombre FROM backup as bac inner JOIN personal as per on per.personalId=bac.personalId ORDER BY `bac`.`backupid` DESC limit 10";
            $resp=$this->db->query($strq);
            return $resp;
    }    
    function compraproductos($id){
        $strq="SELECT cod.cantidad,pro.nombre,cod.precio_compra 
                FROM compras_detalles as cod
                inner join productos as pro on pro.productoid=cod.productoid
                WHERE cod.compraId=$id";
            $resp=$this->db->query($strq);
            return $resp;
    }
    function getdatoscompras($idcompra){
        $strq="SELECT com.compraId, prov.razon_social,com.sucursalid,com.monto_total,com.reg
            FROM compras as com
            inner join proveedores as prov on prov.id_proveedor=com.id_proveedor
            WHERE com.compraId=$idcompra";
        $resp=$this->db->query($strq);
            return $resp;
    }
    function getdatoscomprasd($idcompra){
        $strq="SELECT comd.cantidad,pro.nombre,comd.precio_compra 
                FROM compras_detalles as comd
                inner join productos as pro on pro.productoid=comd.productoid
                WHERE comd.compraId=$idcompra";
        $resp=$this->db->query($strq);
            return $resp;
    }
}
